#include "GameEngine.h"
#include "RenderSystem.h"
#include "FileSystem.h"
#include "InputManager.h"
#include "AssetManager.h"
#include "GameObjectManager.h"


void GameEngine::initialize()
{
	RenderSystem::Instance().initialize(m_settingFile);
	FileSystem::Instance().initialize();
	FileSystem::Instance().initialize();
	InputManager::Instance().initialize();
	AssetManager::Instance().initialize();
	GameObjectManager::Instance().initialize();
}

void GameEngine::GameLoop()
{
	RenderSystem::Instance().update();
	FileSystem::Instance().update();
	FileSystem::Instance().update();
	InputManager::Instance().update();
	AssetManager::Instance().update();
	GameObjectManager::Instance().update();
}
