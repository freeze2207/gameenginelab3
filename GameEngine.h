#pragma once
#ifndef GAMEENGINE_H
#define GAMEENGINE_H

#include <string>

class GameEngine
{
public:
	inline static GameEngine& Instance()
	{
		static GameEngine instance;
		return instance;
	}

	void initialize();
	void GameLoop();

private:
	std::string m_settingFile = "RenderSystem.json";

	inline explicit GameEngine() = default;
	inline ~GameEngine() = default;
	inline explicit GameEngine(GameEngine const&) = delete;
	inline GameEngine& operator=(GameEngine const&) = delete;
};

#endif // !GAMEENGINE_H
