#pragma once
#ifndef INPUTMANAGER_H
#define INPUTMANAGER_H

#include "ISystem.h"

class InputManager : public ISystem
{

public:
	inline static InputManager& Instance()
	{
		static InputManager instance;
		return instance;
	}


protected:
	void initialize() override;
	void update() override;

private:
	friend class GameEngine;

	inline explicit InputManager() = default;
	inline ~InputManager() = default;
	inline explicit InputManager(InputManager const&) = delete;
	inline InputManager& operator=(InputManager const&) = delete;
};

#endif // ! INPUTMANAGER_H
