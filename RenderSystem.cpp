#include "RenderSystem.h"

void RenderSystem::addRenderable(IRenderable* component)
{
	renderComponents.push_back(component);
}

void RenderSystem::removeRenderable(IRenderable* component)
{
	renderComponents.remove(component);
}

void RenderSystem::initialize()
{
	name = "";
	width = 0;
	height = 0;
	fullscreen = false;
	renderComponents.clear();
}

void RenderSystem::initialize(std::string settingFile)
{
	loadSettings(settingFile);
}

void RenderSystem::update()
{
}

void RenderSystem::loadSettings(std::string settingFile)
{
	std::ifstream stream(settingFile);
	nlohmann::json jsonObject;
	stream >> jsonObject;

	if (jsonObject.contains("name"))
	{
		name = jsonObject["name"];
	}
	if (jsonObject.contains("width"))
	{
		width = jsonObject["width"];
	}
	if (jsonObject.contains("height"))
	{
		height = jsonObject["height"];
	}
	if (jsonObject.contains("fullscreen"))
	{
		fullscreen = jsonObject["fullscreen"];
	}

}
