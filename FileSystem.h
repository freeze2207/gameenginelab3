#pragma once
#ifndef FILESYSTEM_H
#define FILESYSTEM_H

#include <string>
#include "ISystem.h"

class FileSystem : public ISystem
{

public:
	inline static FileSystem& Instance()
	{
		static FileSystem instance;
		return instance;
	}


protected:
	void initialize() override;
	void update() override;
	void load(std::string fileName);

private:
	friend class GameEngine;

	inline explicit FileSystem() = default;
	inline ~FileSystem() = default;
	inline explicit FileSystem(FileSystem const&) = delete;
	inline FileSystem& operator=(FileSystem const&) = delete;
};

#endif // ! FILESYSTEM_H

