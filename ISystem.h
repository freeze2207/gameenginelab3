#pragma once
#ifndef ISYSTEM_H
#define ISYSTEM_H


class ISystem
{
protected:
	ISystem();
	~ISystem();

	virtual void initialize() = 0;
	virtual void update() = 0;

	friend class GameEngine;
};

#endif // !ISYSTEM_H
