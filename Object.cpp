#include "Object.h"

Object::Object()
{
    initialized = false;
    name = "";
    id = 0;
}

bool Object::isInitialized()
{
    return false;
}

std::string Object::getName()
{
    return name;
}

void Object::initialize()
{
}

int Object::getId()
{
    return id;
}
