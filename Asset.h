#pragma once
#ifndef ASSET_H
#define ASSET_H

#include "Object.h"

class Asset : public Object
{
protected:
	Asset();
	~Asset();
};

#endif // !ASSET_H
