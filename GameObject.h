#pragma once
#ifndef GAMEJBJECT_H
#define GAMEJBJECT_H

#include <map>
#include <string>
#include "Object.h"

class Component;

class GameObject : public Object
{
private:
	std::map<std::string, Component*> components;

public:
	GameObject();
	~GameObject();

	void addComponent(Component* component);
	void removeComponent(Component* component);

	void initialize();
	void update();
};

#endif // !GAMEJBJECT_H
