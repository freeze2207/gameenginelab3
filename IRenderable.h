#pragma once
#ifndef IRENDERABLE_H
#define IRENDERABLE_H


class IRenderable
{
protected:
	friend class RenderSystem;

	IRenderable();
	~IRenderable();

	virtual void render() = 0;

};

#endif // !IRENDERABLE
