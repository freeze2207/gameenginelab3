#pragma once
#ifndef OBJECT_H
#define OBJECT_H

#include <string>

class Object
{
private:
	bool initialized = false;
	std::string name = "";;
	int id = 0;

protected:
	Object();
	~Object() = default;

public:
	bool isInitialized();
	std::string getName();
	void initialize();
	int getId();

};

#endif // !OBJECT_H
