#include "Component.h"

Component::Component()
	:Object::Object()
{
}

Component::~Component()
{
}

void Component::initialize()
{
}

void Component::update()
{
}
