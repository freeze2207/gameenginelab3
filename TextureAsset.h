#pragma once
#ifndef TEXTUREASSET_H
#define TEXTUREASSET_H

#include "Asset.h"

class TextureAsset : public Asset
{
protected:
	TextureAsset();
	~TextureAsset();
};

#endif // !TEXTUREASSET_H
