#include "GameObject.h"

GameObject::GameObject()
	:Object::Object()
{
}

GameObject::~GameObject()
{
}

void GameObject::addComponent(Component* component)
{
	// name as first parameter?
	components.emplace(Object::getName(), component);
}

void GameObject::removeComponent(Component* component)
{
	components.erase(Object::getName());
}

void GameObject::initialize()
{
	components.clear();
}

void GameObject::update()
{
}
