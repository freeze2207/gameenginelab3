#pragma once
#ifndef RENDERSYSTEM_H
#define RENDERSYSTEM_H
#include <list>
#include <fstream> 
#include <string>
#include "json.hpp"
#include "ISystem.h"

class IRenderable;

class RenderSystem : public ISystem
{

public:
	inline static RenderSystem& Instance()
	{
		static RenderSystem instance;
		return instance;
	}

	void addRenderable(IRenderable* component);
	void removeRenderable(IRenderable* component);

protected:
	void initialize() override;
	void initialize(std::string settingFile);
	void update() override;

private:
	friend class GameEngine;
	std::list<IRenderable*> renderComponents;
	std::string name = "";
	int width = 0;
	int height = 0;
	bool fullscreen = false;

	void loadSettings(std::string settingFile);

	inline explicit RenderSystem() = default;
	inline ~RenderSystem() = default;
	inline explicit RenderSystem(RenderSystem const&) = delete;
	inline RenderSystem& operator=(RenderSystem const&) = delete;
};

#endif // ! RENDERSYSTEM_H
