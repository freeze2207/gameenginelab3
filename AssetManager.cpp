#include "AssetManager.h"

void AssetManager::initialize()
{
	assets.clear();
}

void AssetManager::update()
{
}

void AssetManager::addAsset(Asset* component)
{
	assets.push_back(component);
}

void AssetManager::removeAsset(Asset* component)
{
	assets.remove(component);
}
