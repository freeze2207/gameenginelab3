#pragma once
#ifndef SPRITE_H
#define SPRITE_H

#include <string>

#include "Component.h"
#include "IRenderable.h"

class Sprite : public Component, public IRenderable
{
public:
	Sprite();
	~Sprite();

	int getComponentId() override;
protected:
	void render() override;
};

#endif // !SPRITE_H
