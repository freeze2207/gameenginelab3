#pragma once
#ifndef COMPONENT_H
#define COMPONENT_H

#include <string>
#include "Object.h"

class Component : public Object
{
private:

protected:
	Component();
	~Component();

public:
	void initialize();
	void update();
	virtual int getComponentId() = 0;
};

#endif // !COMPONENT_H
