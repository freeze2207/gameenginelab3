#include "GameObjectManager.h"
#include "GameObject.h"

void GameObjectManager::initialize()
{
    gameObjects.clear();
}

void GameObjectManager::update()
{
}

void GameObjectManager::addGameObject(GameObject* component)
{
    gameObjects.push_back(component);
}

void GameObjectManager::removeGameObject(GameObject* component)
{
    gameObjects.remove(component);
}

GameObject* GameObjectManager::findGameObjectById(int id)
{
    for (std::list<GameObject*>::iterator it = gameObjects.begin(); it != gameObjects.end(); it++)
    {
        if ((*it)->getId() == id)
        {
            return (*it);
        }
        
    }
    return nullptr;
}
