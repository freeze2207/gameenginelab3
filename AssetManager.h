#pragma once
#ifndef ASSETMANAGER_H
#define ASSETMANAGER_H

#include <list>
#include "ISystem.h"

class Asset;

class AssetManager : public ISystem
{
private:
	friend class GameEngine;

	std::list<Asset*> assets;

	inline explicit AssetManager() = default;
	inline ~AssetManager() = default;
	inline explicit AssetManager(AssetManager const&) = delete;
	inline AssetManager& operator=(AssetManager const&) = delete;

protected:
	void initialize() override;
	void update() override;

public:
	void addAsset(Asset* component);
	void removeAsset(Asset* component);


	inline static AssetManager& Instance()
	{
		static AssetManager instance;
		return instance;
	}
};
#endif // !ASSETMANAGER_H

