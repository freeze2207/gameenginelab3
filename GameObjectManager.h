#pragma once
#ifndef GAMEOBJECTMANAGER_H
#define GAMEOBJECTMANAGER_H

#include <list>
#include "ISystem.h"

class GameObject;

class GameObjectManager : public ISystem
{
private:
	friend class GameEngine;
	std::list<GameObject*> gameObjects;

	inline explicit GameObjectManager() = default;
	inline ~GameObjectManager() = default;
	inline explicit GameObjectManager(GameObjectManager const&) = delete;
	inline GameObjectManager& operator=(GameObjectManager const&) = delete;

protected:
	void initialize() override;
	void update() override;

public:
	inline static GameObjectManager& Instance()
	{
		static GameObjectManager instance;
		return instance;
	}

	void addGameObject(GameObject* component);
	void removeGameObject(GameObject* component);

	GameObject* findGameObjectById(int id);

};

#endif // !GAMEOBJECTMANAGER_H
